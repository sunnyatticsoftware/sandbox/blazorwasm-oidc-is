# blazorwasm-oidc-is

Standalone Blazor WASM with OIDC provided by Identity Server.

See https://learn.microsoft.com/en-us/aspnet/core/blazor/security/webassembly/standalone-with-authentication-library?view=aspnetcore-7.0&tabs=visual-studio for further details

## Steps

### Create Blazor WASM
Create a Blazor WASM standalone application. It'll run on 5020 with Http.

### Add WebAssembly Authentication Package
Install the nuget dependency `Microsoft.AspNetCore.Components.WebAssembly.Authentication`, which provides primitives
that help the app authenticate users and obtain tokens to call protected APIs.

Configure OIDC provider pointing to an Identity Server for a registered client Id and providing URLs for the response type `code`
``` 
"Local": {
  "Authority": "https://demo.duendesoftware.com/",
  "ClientId": "interactive.public",
  "PostLogoutRedirectUri": "http://localhost:5020/authentication/logout-callback",
  "RedirectUri": "http://localhost:5020/authentication/login-callback",
  "ResponseType": "code"
}
```

Register the OIDC Authentication in the DI Container
```
services.AddOidcAuthentication(options =>
{
	builder.Configuration.Bind("Local", options.ProviderOptions);
});
```

Add a global import in `_Imports.razor`
```
@using Microsoft.AspNetCore.Components.Authorization
```

Create a `LoginDisplay.razor` component that will be rendered in the Main Layout and will display current name for authenticated users,
it'll offer a button to log out.
For unauthenticated users it offers a log in option.
```
@using Microsoft.AspNetCore.Components.Authorization
@using Microsoft.AspNetCore.Components.WebAssembly.Authentication
@inject NavigationManager Navigation

<AuthorizeView>
    <Authorized>
        Hello, @context.User.Identity?.Name!
        <button class="nav-link btn btn-link" @onclick="BeginLogOut">Log out</button>
    </Authorized>
    <NotAuthorized>
        <a href="authentication/login">Log in</a>
    </NotAuthorized>
</AuthorizeView>

@code{
    private void BeginLogOut()
    {
        Navigation.NavigateToLogout("authentication/logout");
    }
}
```

Create a `Authorization.razor` page component with the routes required for handling authentication stages under path
`"/authentication/{action}"`. This component will manage the appropriate actions at each stage of authentication, and
the redirect URIs point to this.
```
@page "/authentication/{action}"
@using Microsoft.AspNetCore.Components.WebAssembly.Authentication
<RemoteAuthenticatorView Action="@Action"/>

@code{

    [Parameter]
    public string? Action { get; set; }

}
```

Create a `RedirectToLogin.razor` component
```
@using Microsoft.AspNetCore.Components.WebAssembly.Authentication
@inject NavigationManager Navigation
<p>You are being redirected to login...</p>
@code {

    protected override void OnInitialized()
    {
        Navigation.NavigateToLogin("authentication/login");
    }

}
```

In `App.razor` add
```
<CascadingAuthenticationState>
    <Router AppAssembly="@typeof(App).Assembly">
        <Found Context="routeData">
            <AuthorizeRouteView RouteData="@routeData" DefaultLayout="@typeof(MainLayout)">
                <NotAuthorized>
                    @if (context.User.Identity?.IsAuthenticated != true)
                    {
                        <h1>You are not authenticated!</h1>
                        <RedirectToLogin/>
                    }
                    else
                    {
                        <p role="alert">You are not authorized to access this resource.</p>
                    }
                </NotAuthorized>
            </AuthorizeRouteView>
            <FocusOnNavigate RouteData="@routeData" Selector="h1"/>
        </Found>
        <NotFound>
            <PageTitle>Not found</PageTitle>
            <LayoutView Layout="@typeof(MainLayout)">
                <p role="alert">Sorry, there's nothing at this address.</p>
            </LayoutView>
        </NotFound>
    </Router>
</CascadingAuthenticationState>
```

### Add Bearer Token
Add nuget package `Microsoft.Extensions.Http`

Add a custom `AuthorizationMessageHandler`
```
public class CustomAuthenticationMessageHandler
	: AuthorizationMessageHandler
{
	public CustomAuthenticationMessageHandler(IAccessTokenProvider provider, NavigationManager navigation) 
		: base(provider, navigation)
	{
		var authorizedUrls =
			new List<string>()
			{
				"https://sasw-test.free.beeceptor.com"
			};

		ConfigureHandler(authorizedUrls);
	}
}
```

Register HttpClient for a specific name, and make sure the http client is instantiated for that specific name.
When the base url matches the custom authentication message handler, a Bearer token will automatically be added.
```
services
	.AddHttpClient(
		"api",
		configureClient => configureClient.BaseAddress = new Uri("https://sasw-test.free.beeceptor.com"))
	.AddHttpMessageHandler<CustomAuthenticationMessageHandler>();
services.AddScoped(sp => sp.GetRequiredService<IHttpClientFactory>().CreateClient("api"));
services.AddScoped<CustomAuthenticationMessageHandler>();

//services.AddScoped(_ => new HttpClient {BaseAddress = new Uri(builder.HostEnvironment.BaseAddress)});
```

Now you can secure all the Blazor WASM application by adding an `[Authorize]` attribute to `_Imports.razor`
```
@using Microsoft.AspNetCore.Authorization
@attribute [Authorize]
```

Make sure you allow anonymous access to the `Authentication.razor` component or redirections won't work as expected
```
@using Microsoft.AspNetCore.Components.WebAssembly.Authentication
@attribute [AllowAnonymous]
```