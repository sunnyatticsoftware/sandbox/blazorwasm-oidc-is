﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;

namespace BlazorAuth;

public class CustomAuthenticationMessageHandler
	: AuthorizationMessageHandler
{
	public CustomAuthenticationMessageHandler(IAccessTokenProvider provider, NavigationManager navigation) 
		: base(provider, navigation)
	{
		var authorizedUrls =
			new List<string>()
			{
				"https://sasw-test.free.beeceptor.com"
			};

		ConfigureHandler(authorizedUrls);
	}
}