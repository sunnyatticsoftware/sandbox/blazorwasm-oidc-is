using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using BlazorAuth;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

var services = builder.Services;

// OIDC
services.AddOidcAuthentication(options =>
{
	builder.Configuration.Bind("Local", options.ProviderOptions);
});

services
	.AddHttpClient(
		"api",
		configureClient => configureClient.BaseAddress = new Uri("https://sasw-test.free.beeceptor.com"))
	.AddHttpMessageHandler<CustomAuthenticationMessageHandler>();
services.AddScoped(sp => sp.GetRequiredService<IHttpClientFactory>().CreateClient("api"));
services.AddScoped<CustomAuthenticationMessageHandler>();

//services.AddScoped(_ => new HttpClient {BaseAddress = new Uri(builder.HostEnvironment.BaseAddress)});


await builder.Build().RunAsync();